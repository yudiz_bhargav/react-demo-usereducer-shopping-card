import React, { useState, useContext, useReducer, useEffect } from 'react';
import cartItems from './data';
import reducer from './reducer';
import ACTIONS from './ACTIONS';

const url = 'https://course-api.com/react-useReducer-cart-project';
const AppContext = React.createContext();

const initialState = {
  loading: false,
  cart: cartItems,
  total: 0,
  amount: 0,
};

const AppProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const clearCart = () => {
    dispatch({ type: ACTIONS.CLEAR_CART });
  };

  const remove = (id) => {
    dispatch({ type: ACTIONS.REMOVE, payload: id });
  };

  const increase = (id) => {
    dispatch({ type: ACTIONS.INCREASE, payload: id });
  };

  const decrease = (id) => {
    dispatch({ type: ACTIONS.DECREASE, payload: id });
  };

  const fetchData = async () => {
    dispatch({ type: ACTIONS.LOADING });
    const response = await fetch(url);
    const cart = await response.json();
    dispatch({ type: ACTIONS.DISPLAY_ITEMS, payload: cart });
  };

  const toggleAmount = (id, type) => {
    dispatch({ type: ACTIONS.TOGGLE_AMOUNT, payload: { id, type } });
  };

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    dispatch({ type: ACTIONS.GET_TOTALS });
  }, [state.cart]);

  return (
    <AppContext.Provider
      value={{
        ...state, // destructuring the state right away so all properties can be accessed without using state. syntax!!
        clearCart,
        remove,
        increase,
        decrease,

        toggleAmount,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};
// make sure use
export const useGlobalContext = () => {
  return useContext(AppContext);
};

export { AppContext, AppProvider };

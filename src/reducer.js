import ACTIONS from './ACTIONS';

const reducer = (state, action) => {
  if (action.type === ACTIONS.CLEAR_CART) {
    return { ...state, cart: [] };
  }
  if (action.type === ACTIONS.REMOVE) {
    const filteredArray = state.cart.filter((cartItem) => {
      return cartItem.id !== action.payload;
    });
    return { ...state, cart: filteredArray };
  }
  // if (action.type === ACTIONS.INCREASE) {
  //   const tempCart = state.cart.map((cartItem) => {
  //     if (cartItem.id === action.payload) {
  //       return { ...cartItem, amount: cartItem.amount + 1 };
  //     }
  //     return cartItem;
  //   });
  //   return { ...state, cart: tempCart };
  // }
  // if (action.type === ACTIONS.DECREASE) {
  //   const tempCart = state.cart
  //     .map((cartItem) => {
  //       if (cartItem.id === action.payload) {
  //         return { ...cartItem, amount: cartItem.amount - 1 };
  //       }
  //       return cartItem;
  //     })
  //     .filter((cartItem) => cartItem.amount !== 0);
  //   return { ...state, cart: tempCart };
  // }
  if (action.type === ACTIONS.GET_TOTALS) {
    let { total, amount } = state.cart.reduce(
      (cartTotal, cartItem) => {
        const { price, amount } = cartItem;
        const itemTotal = price * amount;

        cartTotal.total += itemTotal;

        cartTotal.amount += amount;
        return cartTotal;
      },
      {
        total: 0,
        amount: 0,
      }
    );
    total = parseFloat(total.toFixed(2));

    return { ...state, total, amount };
  }
  if (action.type === ACTIONS.LOADING) {
    return { ...state, loading: true };
  }
  if (action.type === ACTIONS.DISPLAY_ITEMS) {
    return { ...state, cart: action.payload, loading: false };
  }
  if (action.type === ACTIONS.TOGGLE_AMOUNT) {
    let tempCart = state.cart
      .map((cartItem) => {
        if (cartItem.id === action.payload.id) {
          if (action.payload.type === 'increment') {
            return { ...cartItem, amount: cartItem.amount + 1 };
          }
          if (action.payload.type === 'decrement') {
            return { ...cartItem, amount: cartItem.amount - 1 };
          }
        }
        return cartItem;
      })
      .filter((cardItem) => cardItem.amount !== 0);
    return { ...state, cart: tempCart };
  }
  throw new Error('no matching action type');
};

export default reducer;
